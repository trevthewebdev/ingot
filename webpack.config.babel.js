const path = require('path');
const CssEntryPlugin = require('css-entry-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const webpack = require('webpack');

module.exports = function(env) {
  const isProd = env.production;

  return {
    watch: !isProd,
    watchOptions: {
      ignored: /node_modules/
    },

    entry: {
      ingot: './src/scss/ingot.scss'
    },

    output: {
      filename: (isProd) ? '[name].[chunkhash].js' : '[name].js',
      path: path.resolve(__dirname, 'dist')
    },

    plugins: [
      new ManifestPlugin(),
      new webpack.optimize.CommonsChunkPlugin({
        name: ['manifest']
      }),

      new StyleLintPlugin({
        context: 'src/scss',
        styntax: 'scss'
      }),

      new CssEntryPlugin({
        extensions: ['.css', '.scss'],
        output: {
          filename: (isProd) ? '[name].[contenthash].css' : '[name].css'
        }
      }),
    ],

    module: {
      loaders: [
        {
          test: /\.scss$/,
          loader: ['css-loader', 'sass-loader']
        }
      ]
    }
  }
};
